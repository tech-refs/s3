
# S3 references

## Docker

[S3 launch](docker/base-setup/README.md)

Steps to run docker with s3

[S3 launch and test client on go](docker/go-test/README.md)

Steps to run docker-compose with s3 and tests for go-client

## Go

[Send and get file from S3](go/writereadfile/Readme.md)

Reference of using aws/aws-sdk-go for sending and getting file from S3

