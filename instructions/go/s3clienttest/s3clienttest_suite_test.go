package s3client_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestS3clienttest(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "S3clienttest Suite")
}
