# How to run minio docker image

[Prepare manual for running S3 node](../../docker/base-setup/README.md)

## Go run

```sh
cd $GOPATH/src/gitlab.com/tech-refs/s3/instructions/go/writereadfile && \
go get ./... && \
go run main.go && \
echo "" && \
cat out_test.txt
```