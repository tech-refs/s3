package main

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"os"

	"github.com/aws/aws-sdk-go/aws/awsutil"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

const (
	awsaccesskeyid     = "AKIAIOSFODNN7EXAMPLE"
	awssecretaccesskey = "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"
	filename           = "test.txt"
	myBucket           = "testbucket"
	outFileName        = "out_test.txt"
)

func downloader(svc *s3.S3, filename, myBucket, outFileName string) error {
	path := fmt.Sprintf("/%s/%s", myBucket, filename)
	out, err := svc.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(myBucket),
		Key:    aws.String(path),
	})
	if err != nil {
		return fmt.Errorf("Getting object %s from bucket %s failed. Error: %s", filename, myBucket, err)
	}
	outFile, err := os.Create(outFileName)
	if err != nil {
		return fmt.Errorf("Creating file %s failed. Error: %s", outFileName, err)
	}
	defer outFile.Close()
	_, err = io.Copy(outFile, out.Body)
	if err != nil {
		return fmt.Errorf("Saving  object %s failed. Error: %s", outFile.Name(), err)
	}
	fmt.Printf("File %s saved successfully", outFile.Name())
	return nil
}

func uploader(svc *s3.S3, filename, myBucket string) error {
	file, err := os.Open(filename)
	if err != nil {
		return fmt.Errorf("failed to open file %q, %v", filename, err)
	}
	defer file.Close()
	fileInfo, _ := file.Stat()
	size := fileInfo.Size()
	buffer := make([]byte, size) // read file content to buffer
	file.Read(buffer)
	fileBytes := bytes.NewReader(buffer)
	fileType := http.DetectContentType(buffer)
	path := fmt.Sprintf("/%s/%s", myBucket, filename)
	params := &s3.PutObjectInput{
		Bucket:        aws.String(myBucket),
		Key:           aws.String(path),
		Body:          fileBytes,
		ContentLength: aws.Int64(size),
		ContentType:   aws.String(fileType),
	}
	resp, err := svc.PutObject(params)
	if err != nil {
		panic(err)
	}
	fmt.Printf("response %s", awsutil.StringValue(resp))
	return nil
}

func main() {

	token := ""
	creds := credentials.NewStaticCredentials(awsaccesskeyid, awssecretaccesskey, token)
	_, err := creds.Get()
	if err != nil {
		panic(err)
	}
	cfg := aws.NewConfig().WithRegion("us-east-1").WithCredentials(creds).WithEndpoint("http://localhost:9000")
	svc := s3.New(session.New(), cfg)

	err = uploader(svc, filename, myBucket)
	if err != nil {
		panic(err)
	}

	err = downloader(svc, filename, myBucket, outFileName)
	if err != nil {
		panic(err)
	}
}
