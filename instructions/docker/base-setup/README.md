# How to run S3 docker image

## Run minio local host in docker

```sh
docker stop minio || true && \
docker rm minio || true && \
docker run \
    -d \
    -p 9000:9000 \
    --name minio \
    -e "MINIO_ACCESS_KEY=SOMEACCESSKEY" \
    -e "MINIO_SECRET_KEY=SOMESECRETKEY" \
    minio/minio server /data
```

## S3 install

```sh
wget -O- -q http://s3tools.org/repo/deb-all/stable/s3tools.key | sudo apt-key add - && \
sudo wget -O/etc/apt/sources.list.d/s3tools.list http://s3tools.org/repo/deb-all/stable/s3tools.list && \
sudo apt-get update && sudo apt-get install -y s3cmd
```

## S3 config

```sh
rm -rf ~/.s3cfg
echo "# Setup endpoint" >> ~/.s3cfg && \
echo "host_base = localhost:9000" >> ~/.s3cfg && \
echo "host_bucket = %(bucket).localhost:9000" >> ~/.s3cfg && \
echo "bucket_location = us-east-1" >> ~/.s3cfg && \
echo "use_https = False" >> ~/.s3cfg && \
echo "" >> ~/.s3cfg && \
echo "# Setup access keys" >> ~/.s3cfg && \
echo "access_key = SOMEACCESSKEY" >> ~/.s3cfg && \
echo "secret_key = SOMESECRETKEY" >> ~/.s3cfg && \
echo "" >> ~/.s3cfg && \
echo "# Enable S3 v4 signature APIs" >> ~/.s3cfg && \
echo "signature_v2 = False" >> ~/.s3cfg && \
cat ~/.s3cfg
```

## Add subdomain to hosts

```sh
sudo -- sh -c -e "echo '127.0.0.1       testbucket.localhost' >> /etc/hosts" && \
cat /etc/hosts
```

## S3 working with files (s3cmd)

## Example and preparing for go references

```sh
s3cmd rb s3://testbucket --region="us-east-1" || true && \
s3cmd mb s3://testbucket --region="us-east-1" && \
s3cmd ls && \
cd $GOPATH/src/gitlab.com/tech-refs/s3/instructions/docker/s3 && \
rm -r tmp || true && \
mkdir -p tmp && \
echo -e "hello world" > tmp/test.txt && \
s3cmd put tmp/test.txt s3://testbucket && \
s3cmd get s3://testbucket/test.txt tmp/test_again.txt && \
cat tmp/test_again.txt && \
s3cmd del s3://testbucket/test.txt
```

## Sync folder with s3 bucket without config

```sh
s3cmd sync . s3://testbucket \
    --host=localhost:9000 \
    --host-bucket="%(bucket).localhost:9000" \
    --access_key=SOMEACCESSKEY \
    --secret_key=SOMESECRETKEY
```

## Removing bucket (don't do it if you whant run go references)

```sh
s3cmd rb s3://testbucket
```

## Run scality S3 local host in docker (deprecated)

```sh
docker stop s3server || true && \
docker rm s3server || true && \
docker run -d \
    --name s3server \
    -e IPFS_PROFILE=test \
    -p 8000:8000 \
    -p 9990:9990 \
    -p 9991:9991 \
    scality/s3server:latest
```