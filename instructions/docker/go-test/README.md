# Run test in docker

## Up Docker compose

```sh
cd $GOPATH/src/gitlab.com/tech-refs/s3/instructions/docker/go-test && \
docker-compose down || true && \
docker-compose up --build --exit-code-from go_test go_test
```

## Check exit code

```sh
echo $?
```