package s3client

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awsutil"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

// S3client represents S3 client
type S3client struct {
	client *s3.S3
}

// New return new instance of the s3 client with session
func New(accesskey, secretaccesskey, token, region, endpoint string) (*S3client, error) {
	creds := credentials.NewStaticCredentials(accesskey, secretaccesskey, token)
	_, err := creds.Get()
	if err != nil {
		return nil, fmt.Errorf("Getting credentials faulse. Error: %s", err)
	}
	cfg := aws.NewConfig().WithRegion(region).WithCredentials(creds).WithEndpoint(endpoint)
	s3client := &S3client{
		client: s3.New(session.New(), cfg),
	}
	return s3client, nil
}

// Upload saves filename to bucket in S3 storage
func (c *S3client) Upload(filename, bucket string) error {
	file, err := os.Open(filename)
	if err != nil {
		return fmt.Errorf("failed to open file %q, %v", filename, err)
	}
	defer file.Close()
	fileInfo, _ := file.Stat()
	size := fileInfo.Size()
	buffer := make([]byte, size) // read file content to buffer
	file.Read(buffer)
	fileBytes := bytes.NewReader(buffer)
	fileType := http.DetectContentType(buffer)
	path := fmt.Sprintf("/%s/%s", bucket, filename)
	params := &s3.PutObjectInput{
		Bucket:        aws.String(bucket),
		Key:           aws.String(path),
		Body:          fileBytes,
		ContentLength: aws.Int64(size),
		ContentType:   aws.String(fileType),
	}
	resp, err := c.client.PutObject(params)
	if err != nil {
		panic(err)
	}
	fmt.Printf("response %s", awsutil.StringValue(resp))
	return nil
}

// Download gets filename from bucket and save it into outFileName
func (c *S3client) Download(filename, bucket, outFileName string) error {
	path := fmt.Sprintf("/%s/%s", bucket, filename)
	out, err := c.client.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(path),
	})
	if err != nil {
		return fmt.Errorf("Getting object %s from bucket %s failed. Error: %s", filename, bucket, err)
	}
	outFile, err := os.Create(outFileName)
	if err != nil {
		return fmt.Errorf("Creating file %s failed. Error: %s", outFileName, err)
	}
	defer outFile.Close()
	_, err = io.Copy(outFile, out.Body)
	if err != nil {
		return fmt.Errorf("Saving  object %s failed. Error: %s", outFile.Name(), err)
	}
	fmt.Printf("File %s saved successfully", outFile.Name())
	return nil
}
