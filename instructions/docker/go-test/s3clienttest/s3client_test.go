package s3client_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	s3client "gitlab.com/donum/donum/go/s3client"
)

var _ = Describe("S3client", func() {

	var (
		awsaccesskeyid     = "AKIAIOSFODNN7EXAMPLE"
		awssecretaccesskey = "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"
		token              = ""
		region             = "us-east-1"
		endpoint           = "http://minio:9000"
		filename           = "test.txt"
		bucket             = "testbucket"
		outFileName        = "out_test.txt"
		err                error
	)

	Describe("Init client", func() {
		It("should pass", func() {
			_, err = s3client.New(awsaccesskeyid, awssecretaccesskey, token, region, endpoint)
			Expect(err).To(BeNil())
		})
	})

	var client = &s3client.S3client{}

	BeforeEach(func() {

		client, _ = s3client.New(awsaccesskeyid, awssecretaccesskey, token, region, endpoint)

	})

	Describe("Adding and gettin file from S3 storage", func() {
		It("should pass", func() {
			Expect(client.Upload(filename, bucket)).Should(Succeed())
			Expect(client.Download(filename, bucket, outFileName)).Should(Succeed())
		})
	})
})
