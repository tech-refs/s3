#! /bin/sh
set -e

s3cmd mb s3://testbucket \
    --access_key=$KEY \
    --secret_key=$SECRET \
    --no-ssl \
    --host=$HOST:9000 \
    --host-bucket="%(bucket).$HOST:9000" \
    --region="us-east-1"

