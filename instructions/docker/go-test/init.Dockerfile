FROM garland/docker-s3cmd
COPY init.sh /init/init.sh
WORKDIR /init
RUN chmod +x init.sh
CMD ["./init.sh"]